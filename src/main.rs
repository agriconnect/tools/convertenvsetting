#[macro_use]
extern crate clap;
extern crate toml;
extern crate envfile;
#[macro_use]
extern crate serde_derive;

use std::io;
use std::io::Write;
use std::process::exit;
use std::path::Path;
use std::fs;
use std::fs::File;
use std::collections::HashMap;

use envfile::EnvFile;
use clap::{App, Arg};

#[derive(Serialize)]
struct CustSettings {
    default: HashMap<String, toml::Value>,
}


fn main() {
    let matches = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(Arg::with_name("WORK_FOLDER")
             .help("The application folder to look for settings files")
             .required(true))
        .get_matches();

    let folder_raw = matches.value_of("WORK_FOLDER").unwrap();
    println!("Work on folder: {}", folder_raw);
    let folder = Path::new(folder_raw);
    if !folder.is_dir() {
        writeln!(&mut io::stderr(), "Folder does not exists!").ok();
        exit(1);
    }
    let src = folder.join("project/.env");
    if !src.is_file() {
        writeln!(&mut io::stderr(), "{} file doesn't exist!", src.display()).ok();
        exit(1);
    }
    let dst = folder.join("customer_settings.toml");
    if dst.is_file() {
        println!("{} file exists. Delete it.", dst.display());
        if fs::remove_file(&dst).is_err() {
            writeln!(&mut io::stderr(), "Failed to delete {}!", dst.display()).ok();
            exit(1)
        }
    }
    let evfile = match EnvFile::new(src.as_path()) {
        Err(e) => {
            writeln!(io::stderr(), "Failed to parse .env file! Error: {}", e).ok();
            exit(1);
        },
        Ok(r) => r
    };
    let mut settings = HashMap::new();
    for (k, v) in evfile.store {
        if k == "DATABASE_URL" {
            continue;
        }
        let value = match v.parse::<i32>() {
            Ok(n) => toml::Value::from(n),
            Err(_) => match v.parse::<f64>() {
                Ok(n) => toml::Value::from(n),
                Err(_) => toml::Value::from(v)
            }
        };
        settings.insert(k.to_string(), value);
    }
    let data = CustSettings {
        default: settings
    };
    let final_string = match toml::to_string(&data) {
        Ok(content) => content,
        Err(e) => {
            writeln!(io::stderr(), "Failed to convert to TOML. Error: {}", e).ok();
            exit(1)
        }
    };
    let mut file = match File::create(&dst) {
        Ok(f) => f,
        Err(_) => {
            writeln!(&mut io::stderr(), "Failed to create {}!", dst.display()).ok();
            exit(1)
        }
    };
    match file.write(final_string.as_bytes()) {
        Ok(_) => {
            println!("Convert successfully!")
        },
        Err(e) => {
            writeln!(io::stderr(), "Error while writing to {}. Error: {}", dst.display(), e).ok();
            exit(1)
        }
    }
}
